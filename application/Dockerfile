FROM node:16.14 AS builder
WORKDIR /usr/src/app
COPY . .
RUN npm i
RUN npm run build


FROM node:16.14 AS cleaner
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/dist ./
COPY --from=builder /usr/src/app/package.json ./
COPY --from=builder /usr/src/app/package-lock.json ./
COPY --from=builder /usr/src/app/swagger.yml ./
RUN npm ci --only=production --ignore-scripts


FROM node:16.14-alpine3.15 AS alpine
ENV NODE_ENV=production
RUN apk add --no-cache curl dumb-init
USER node
WORKDIR /usr/src/app
COPY --chown=node:node --from=cleaner /usr/src/app ./
CMD ["dumb-init", "node", "server.js"]

