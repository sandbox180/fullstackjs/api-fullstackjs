import * as path from 'path'

const projectRoot = process.cwd()
const testsRoot = path.resolve(projectRoot, 'white-box-testing', '__tests__')
const integrationsTestsPath = path.resolve(testsRoot, 'integrations')

export default {
  rootDir: integrationsTestsPath,
  coverageProvider: 'v8',
  testEnvironment: 'node',
  roots: [`<rootDir>`],
  preset: 'ts-jest',
  collectCoverage: false,
  modulePathIgnorePatterns: ['global-setup']
}
