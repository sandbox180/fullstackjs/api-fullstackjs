import * as path from 'path'

const projectRoot = process.cwd()

const outputDirPath = path.resolve(projectRoot, 'outputs')
const unitReportPath = path.resolve(outputDirPath, 'unit-report')
const coverageReportPath = path.resolve(outputDirPath, 'unit-coverage')

export default {
  rootDir: projectRoot,
  testEnvironment: 'node',
  roots: ['<rootDir>/src'],
  preset: 'ts-jest',
  reporters: [
    'default',
    [
      'jest-junit',
      {
        addFileAttribute: true,
        suiteName: 'jest JUnit tests report',
        outputDirectory: outputDirPath,
        outputName: 'junit.xml',
        titleTemplate: '{title}',
        classNameTemplate: '{classname}'
      }
    ],
    [
      './node_modules/jest-html-reporter',
      {
        pageTitle: 'Your test suite',
        outputPath: `${unitReportPath}/unit-report.html`,
        includeFailureMsg: true
      }
    ]
  ],
  collectCoverage: false,
  coverageProvider: 'v8',
  coverageDirectory: coverageReportPath,
  coverageReporters: ['cobertura', 'html', 'text-summary'],
  collectCoverageFrom: ['<rootDir>/src/**'],
  coverageThreshold: {
    './src/components': {
      branches: 0,
      functions: 0,
      lines: 0,
      statements: 0
    }
  }
}
