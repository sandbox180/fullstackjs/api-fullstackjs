import jwt from "jsonwebtoken"
import { BusinessError } from "../entities/BusinessError/BusinessError"
import { StatusCodes } from "http-status-codes"
import { IJwtPayload } from "../entities/IJwtPayload"

const { JWT_PUBLIC_KEY } = process.env

export function jwtCheckTokenValidity (tokenId : string) : IJwtPayload {

  try {
    return jwt.verify(
      tokenId,
      JWT_PUBLIC_KEY as string) as IJwtPayload
  }
  catch (err){
    throw new BusinessError(
      "jwt invalid ",
      StatusCodes.UNAUTHORIZED,
      "You should correctly authenticate"
    )
  }

}
