import debuger, { Debugger } from "debug"
import { ReasonPhrases, StatusCodes } from "http-status-codes"
import { BusinessError } from "../../entities/BusinessError/BusinessError"
import { IBusinessError } from "../../entities/BusinessError/IBusinessError"

const debug: Debugger = debuger("api:md_error")

function errorsHandler (err, req, res, next) {

  console.log("____ERROR HANDLEEERRR____")
  if (!err)
    return next()

  if (err instanceof BusinessError) {
    debug("%O", err)
    const busninessError : IBusinessError = {
      error: err.error,
      message: err.message,
      status: err.status,
      details: err.details,
      instance: err.instance
    }
    res.status(err.status).send(busninessError)
  }
  else {
    console.log("unhandled error", err.message)
    res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
      status: StatusCodes.INTERNAL_SERVER_ERROR,
      message: ReasonPhrases.INTERNAL_SERVER_ERROR
    })
  }

}

export default errorsHandler
