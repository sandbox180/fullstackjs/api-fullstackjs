import { connect, disconnect } from "mongoose"
import { IMongoConnect } from "./IMongoConnect"

export async function dbConnexion (dbconnexion: IMongoConnect) {

  await disconnect()
  const {
    hostname,
    dbName,
    user = "",
    pwd = "",
    port = 27017,
    authSource = "",
    dnsSrvRecord = ""
  } = dbconnexion

  const { NODE_ENV } = process.env

  const url = (): string => {

    if (!hostname)
      throw new Error("You should give a mongo hostname for connection")
    else if (!dbName)
      throw new Error("You should give a mongo database name for connection")

    const protocol = dnsSrvRecord ? "mongodb+srv" : "mongodb"
    const auth = user && pwd ? `${user}:${pwd}@` : ""
    const host = `${hostname}:${port}`
    return `${protocol}://${auth}${host}/${dbName}`

  }

  const params = (): string => {

    const params: string[] = []

    if (authSource)
      params.push(`authSource=${authSource}`)

    if (NODE_ENV === "production") {
      params.push("retryWrites=true")
      params.push("w=majority")
    }

    return `?${params.join("&")}`

  }
  const uri = `${url()}${params()}`

  try {
    await connect(uri)
    console.log("SUCCESS MONGODB CONNECTED ON : ", uri)
  }
  catch (err) {
    throw new Error(`ERROR MONGODB CONNECTION !! :
      - Db url is : ${uri}
      - Error is : ${err}`)
  }

}
