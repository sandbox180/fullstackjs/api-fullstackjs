export interface IMongoConnect {
  hostname: string
  dbName: string
  user?: string
  pwd?: string
  port?: string | number
  authSource?: string
  dnsSrvRecord?: string
}
