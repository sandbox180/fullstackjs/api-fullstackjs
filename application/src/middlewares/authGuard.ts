import { BusinessError } from "../entities/BusinessError/BusinessError"
import { StatusCodes } from "http-status-codes"
import { jwtCheckTokenValidity } from "../services/jwtCheckTokenValidity"

function authGuard (req, res, next) : void {

  const tokenValue : string|undefined = req.headers.authorization
  if (!tokenValue) {
    return next(
      new BusinessError(
        "missing jwt ",
        StatusCodes.UNAUTHORIZED,
        "You must be autheticate to access this ressource"
      ))
  }

  const decodedToken = jwtCheckTokenValidity(tokenValue)
  req.jwtDecodedToken =
  next()

}
