import express, { Express } from "express"
import { Router } from "express/ts4.0"
import * as fs from "fs"
import path from "path"
import { load } from "js-yaml"
import swaggerUi from "swagger-ui-express"

class ExpressApp {

  private app = express()

  constructor (private router: Router, private apiName: string = "") {}

  private setHeaders () {
    this.app.use((req, res, next) => {
      res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
      )
      res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PUT, DELETE, PATCH, OPTIONS"
      )
      res.setHeader("Access-Control-Allow-Origin", "*")
      res.setHeader("Accept", "application/json")
      next()
    })
  }

  private setOpenAPI (): void {
    const ymlBuffer = fs.readFileSync(path.join(process.cwd(), "swagger.yml"))
    const swaggerDocument: any = load(ymlBuffer.toString())
    this.router.use(
      "/api-docs",
      swaggerUi.serve,
      swaggerUi.setup(swaggerDocument)
    )
  }

  private setParsers () : void {
    this.app.use(express.urlencoded({ extended: true }))
    this.app.use(express.json())
    this.app.use(express.text())
  }

  public init (): Express {

    this.setOpenAPI()
    this.setHeaders()
    this.setParsers()
    this.app.use(`/${this.apiName}`, this.router)
    return this.app

  }

}

export { ExpressApp }
