class EventPrefixer {

  constructor (private prefix: string) {}

  private prefixer (object: Record<string, string>): any {
    const prefixObject: any = {}
    for (const key in object)
      prefixObject[key] = `${this.prefix}:${object[key]}`
    return prefixObject
  }

  public build<
    C extends Record<string, string>,
    S extends Record<string, string>
    > (client: C, server: S) {

    return {
      client: this.prefixer(client) as C,
      server: this.prefixer(server) as S
    }
  }
}

export const playerEvents = new EventPrefixer("player").build(
  {
    SEND_MESSAGE: "SEND_MESSAGE"
  },
  {
    MESSAGE_RECEIVED: "MESSAGE_RECEIVED"
  }
)
