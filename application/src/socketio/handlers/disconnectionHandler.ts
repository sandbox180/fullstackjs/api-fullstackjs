import { Server, Socket } from "socket.io"
import { ioErrorHandler } from "../../services/ErrorHandlers/ioErrorHandler"

function disconnectionHandler (io: Server, socket: Socket) {

  const leaveRoom = async () => {
    try {
      console.log("user leave room")
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  socket.on("disconnect", leaveRoom)

}

export { disconnectionHandler }
