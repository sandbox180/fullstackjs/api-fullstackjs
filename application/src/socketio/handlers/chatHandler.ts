import { Server, Socket } from "socket.io"
import { ioErrorHandler } from "../../services/ErrorHandlers/ioErrorHandler"
import { playerEvents } from "../Events"

class ChatHandler {

  static sendMessage = (io: Server, socket: Socket) => async (
    message: string,
    username: string,
    jwt: string
  ) => {
    try {
      /*TODO:
      *  - Check authentication
      *  - Create msg entity
      *  - Save in DB
      *  - Emit message broadcast
      * */
      io.emit(playerEvents.server.MESSAGE_RECEIVED, ...["messageData"])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

}

function initInGameHandler (io: Server, socket: Socket) {

  socket.on(playerEvents.client.SEND_MESSAGE, ChatHandler.sendMessage(io, socket))

}

export { initInGameHandler, ChatHandler }
