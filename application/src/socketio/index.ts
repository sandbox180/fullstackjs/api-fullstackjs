import { Server } from "socket.io"
import { Server as httpServer } from "http"
import { disconnectionHandler } from "./handlers/disconnectionHandler"
import { initInGameHandler } from "./handlers/chatHandler"

function socketioInit (server: httpServer) {

  const io: Server = new Server(server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"]
    }
  })

  const onConnection = (socket) => {
    console.log("a user connected:", socket.id)
    initInGameHandler(io, socket)
    disconnectionHandler(io, socket)
  }
  io.on("connection", onConnection)
}

export default socketioInit
