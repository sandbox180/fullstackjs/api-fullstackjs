export interface IPassword {
  id: string
  userId: string
  hash: string
}
