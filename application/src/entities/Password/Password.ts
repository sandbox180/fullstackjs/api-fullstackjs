import { Entity } from "../Entity"
import { IPassword } from "./IPassword"

export class Password extends Entity<IPassword> {

  static create (
    userId: string,
    rawPassword: string
  ): void {

    /*TODO
    *  - Hash password
    *  - Save hashed password in DB
    * */
  }

  static checkPassword (
    userId: string,
    rawPassword: string
  ): boolean {
    /*TODO
    *  - hash rawPassword
    *  - compare with database hashed password
    * */
    return true
  }

}
