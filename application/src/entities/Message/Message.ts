import { Entity } from "../Entity"
import { IMessage } from "./IMessage"
import { IUser } from "../User/IUser"

export class Message extends Entity<Message> {

  static async send (
    user: IUser,
    message: string
  ): Promise<IMessage> {
    /*TODO
    *  - check msg validity
    *  - send message
    * */
    return "" as any
  }

  static async getAll (): Promise<IMessage[]> {
    /*TODO
    *  - check msg validity
    *  - send message
    * */
    return []
  }

  static async remove (
    messageId: string,
    userId: string
  ): Promise<string> {
    /*TODO */
    return ""
  }

}
