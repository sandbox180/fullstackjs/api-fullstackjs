export interface IMessage {
  id: string
  value: string
  ownerId: string
  ownerAvatar: string
  sendAt: Date
}
