import { Entity } from "../Entity"
import { IUser } from "./IUser"
import userModel from "../../models/UserModel"
import { BusinessError } from "../BusinessError/BusinessError"
import { StatusCodes } from "http-status-codes"

export class User extends Entity<IUser> {

  static async findOne (
    username: string
  ): Promise<IUser> {

    if (!username) {
      throw new BusinessError(
        "Invalid data",
        StatusCodes.BAD_REQUEST,
        "You should provide valide username"
      )
    }

    const user : IUser | null = await userModel.findOne({ username }).lean()
    if (!user) {
      throw new BusinessError(
        "Not found user",
        StatusCodes.NOT_FOUND,
        "User does't exist"
      )
    }

    return user

  }

  static async create (
    username: string
  ): Promise<IUser> {

    if (!username) {
      throw new BusinessError(
        "Invalid data",
        StatusCodes.BAD_REQUEST,
        "You should provide valide username"
      )
    }

    const user: IUser = {
      id: User.generateId("user-"),
      username: username,
      avatar: `https://avatars.dicebear.com/api/adventurer/${username}.svg`
    }
    await new userModel(user).save()
    return user

  }

  static async login (
    mail: string,
    pwd: string
  ): Promise<IUser> {
    /*TODO
    *  - Compare Hash
    *  - Return User
    * */

    return "" as any
  }

}
