import { IBusinessError } from "./IBusinessError"

class BusinessError extends Error implements IBusinessError{

  public type = "business" as const

  constructor (
    public error: string,
    public status: number,
    message: string,
    public instance: string = "",
    public details: string = ""
  ) {
    super(message)
  }

}

export { BusinessError }
