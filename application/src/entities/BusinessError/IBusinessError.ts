export interface IBusinessError {
  error: string
  status: number
  message: string,
  instance: string
  details: string
}
