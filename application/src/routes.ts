import express from "express"
import errorsHandler from "./services/ErrorHandlers/restErrorHandler"
import { healthCheckRouter } from "./components/healthCheck/healthCheckRouter"
import { authenticationRouter } from "./components/authentication/authenticationRouter"
import { chatBoxRouter } from "./components/chatBox/chatBoxRouter"

const router = express.Router()

router.use("/healthcheck", healthCheckRouter)
router.use("/auth", authenticationRouter)
router.use("/chatBox", chatBoxRouter)
router.use(errorsHandler)

export { router as rootRouter }
