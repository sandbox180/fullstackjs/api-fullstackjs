import { model, Schema } from "mongoose"
import { IUser } from "../entities/User/IUser"
import { IPassword } from "../entities/Password/IPassword"

const schema = new Schema<IPassword>({
  id: { type: String, required: true, unique: true },
  userId: { type: String, required: true, unique: true },
  hash: { type: String, required: true }
})

const passwordModel = model<IUser>("passwords", schema)
export default passwordModel
