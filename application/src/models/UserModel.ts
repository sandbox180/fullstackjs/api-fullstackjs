import { model, Schema } from "mongoose"
import { IUser } from "../entities/User/IUser"

const schema = new Schema<IUser>({
  id: { type: String, required: true, unique: true },
  username: { type: String, required: true, unique: true },
  avatar: { type: String, required: true }
})

const userModel = model<IUser>("user", schema)
export default userModel
