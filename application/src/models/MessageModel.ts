import { model, Schema } from "mongoose"
import { IMessage } from "../entities/Message/IMessage"

const schema = new Schema<IMessage>({
  id: { type: String, required: true, unique: true },
  value: { type: String, required: true },
  ownerId: { type: String, required: true },
  ownerAvatar: { type: String, required: true },
  sendAt: { type: Date, required: true }
})

const messageModel = model<IMessage>("messages", schema)
export default messageModel
