import { User } from "../../entities/User/User"
import { IUser } from "../../entities/User/IUser"
import { Password } from "../../entities/Password/Password"
import jwt from "jsonwebtoken"
import { StatusCodes } from "http-status-codes"

const { JWT_PRIVATE_KEY } = process.env

export const login = async (req, res, next) => {
  try {
    const { username, password } = req.body

    const user : IUser = await User.findOne(username)
    Password.checkPassword(user.username, password)

    const token = jwt.sign(
      { userId: user.id },
      JWT_PRIVATE_KEY as string,
      { algorithm: "RS256" }
    )

    res.status(StatusCodes.OK).json({ token })
  }
  catch (error) {
    return next(error)
  }
}

export const register = async (req, res, next) => {
  try {
    const { username, password } = req.body

    /*TODO: check username & pwd validity*/

    const user : IUser = await User.create(username)

    await Password.create(user.id, password)
    res.status(StatusCodes.CREATED).json({ message: "Account created" })
  }
  catch (error) {
    return next(error)
  }
}

export const logout = async (req, res, next) => {
  try {
    res.status(StatusCodes.OK).send("TODO")
  }
  catch (error) {
    return next(error)
  }
}
