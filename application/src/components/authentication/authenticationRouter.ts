import express from "express"
import { login, logout, register } from "./authenticationController"

const router = express.Router()

router.post("/login", login)
router.post("/register", register)
router.get("/logout", logout)
export { router as authenticationRouter }
