import express from "express"
import { getAll, getOne } from "./chatBoxController"

const router = express.Router()

router.get("/message", getAll)
router.get("/message/:messageId", getOne)
export { router as chatBoxRouter }
