import express from "express"
import { healthchecking } from "./healthCheckController"

const router = express.Router()

router.get("/", healthchecking)
export { router as healthCheckRouter }
