export const healthchecking = async (req, res, next) => {
  try {
    res.status(200).send("ok")
  }
  catch (error) {
    return next(error)
  }
}
